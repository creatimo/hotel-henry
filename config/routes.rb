Rails.application.routes.draw do

  get '/calculate', to: 'bookings#calculate'
  get '/timeline_data', to: 'admin/bookings#timeline_data'

  get '/timeline_groups', to: 'admin/bookings#timeline_groups'
  get '/timeline_items', to: 'admin/bookings#timeline_items'

  scope '/:locale' do
    post 'bookings/room', to: 'bookings#room', as: 'room_booking'
    get 'bookings/success', to: 'bookings#success', as: 'success_booking'

    get 'hh/:key', to: 'pages#show', as: 'page'

    resources :bookings
    resources :room_bookings
    resources :rooms
    devise_for :users
    resources :users

    get 'kontakty', to: 'static_pages#contacts', as: 'contacts'
    get 'spa', to: 'static_pages#spa', as: 'spa'
    get 'restaurace', to: 'static_pages#restaurant', as: 'restaurant'
    get 'pokoj', to: 'static_pages#rooms', as: 'pokoje'

    root to: 'static_pages#homepage'

    namespace :admin do
      resources :pages
      resources :pictures
      resources :bookings
      resources :rooms
      resources :users

      get 'timeline', to: 'bookings#timeline', as: 'timeline'
    end

  end

  get '/', to: 'static_pages#homepage'

end
