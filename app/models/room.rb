class Room < ActiveRecord::Base
  enum room_type: [:room, :service]
  after_initialize :set_default_type, :if => :new_record?

  has_many :room_bookings

  def set_default_type
    self.room_type ||= :room
  end

end
