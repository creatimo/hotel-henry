class Booking < ActiveRecord::Base
  has_many :room_booking, dependent: :destroy
  belongs_to :user
end
