class Page < ActiveRecord::Base
  enum language: [ :language_draft, :cs, :en ]
  enum web: [ :web_draft, :standard, :hours ]

  has_many :pictures, :dependent => :destroy

  accepts_nested_attributes_for :pictures, allow_destroy: true, reject_if: lambda { |a| a[:file].blank? }
end
