class Picture < ActiveRecord::Base
  belongs_to :page

  has_attached_file :image,
    :styles => {
      :carousel => "1280x380#",
      :large => "750x417#",
      :medium => "300x300#",
      :thumb => "120x120#"
    },
    :path => "/pages/picures/:id/:basename_:style.:extension",
    :url =>':s3_domain_url'

  do_not_validate_attachment_file_type :image
end
