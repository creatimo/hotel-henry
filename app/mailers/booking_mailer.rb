class BookingMailer < ApplicationMailer
  default from: "no-reply@privathenry.cz"

  def new_booking(booking)
    @booking = booking
    mail(to: Rails.application.secrets.info_mail, subject: "Nová rezervace: #{booking.user.phone}")
  end
end
