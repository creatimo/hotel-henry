json.array!(@room_bookings) do |room_booking|
  json.extract! room_booking, :id, :room_id, :booking_id, :book_from, :book_to
  json.url room_booking_url(room_booking, format: :json)
end
