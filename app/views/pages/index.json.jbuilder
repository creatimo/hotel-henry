json.array!(@pages) do |page|
  json.extract! page, :id, :title, :perex, :content, :language_id, :web_id, :key
  json.url page_url(page, format: :json)
end
