class Admin::PagesController < ApplicationController
  before_filter lambda { @body_class = 'admin' }
  before_action :set_page, only: [:edit, :update]
  before_action :authenticate_user!
  before_action :admin_only
  def index
    @pages = Page.all
  end

  def edit
  end

  def update

    if params[:images]
      params[:images].each { |image|
        @page.pictures.create(image: image)
      }
    end

    if @page.update(page_params)
      redirect_to edit_admin_page_path(@page), notice: 'Page was successfully updated.'
    else
      render :edit
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:title, :perex, :content, :language, :web, :key)
    end

    def admin_only
      unless current_user.admin?
        redirect_to :back, :alert => "Access denied."
      end
    rescue ActionController::RedirectBackError
      redirect_to root_path
    end

end
