class Admin::PicturesController < ApplicationController
  def destroy
    @picture = Picture.find(params[:id])
    @page = @picture.page
    @picture.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end

  def delete
    @picture = Picture.find(params[:id])
    @page = @picture.page
    @picture.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end
end
