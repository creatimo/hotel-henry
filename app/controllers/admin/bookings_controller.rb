class Admin::BookingsController < ApplicationController
  before_filter lambda { @body_class = 'admin' }
  before_action :set_booking, only: [:show, :edit, :update, :destroy]

  def index
    @bookings = Booking.all.order('id DESC')
  end

  def show
  end

  def timeline
    @bookings = Booking.all
    @rooms = Room.all
  end

  def timeline_groups
    rooms = Array.new
    Room.all.each do |room|
      room_data = {
        id: room.id,
        content: room.name
      }
      rooms << room_data
    end
    render json: rooms
  end

  def timeline_items
    bookings = Array.new
    RoomBooking.all.each do |book|
      booking_data = {
        # -2 hours fix bug in vis timeline
        start: "/Date(#{book.book_from.to_time.utc.to_i - (60*60*2)}000)/",
        end: "/Date(#{book.book_to.to_time.utc.to_i - (60*60*2)}000)/",
        content: "<a href=\"#{admin_booking_path(book.booking_id)}\">detail</a>",
        group: "#{book.room_id}",
        title: "#{l book.book_from.utc, format: :short} - #{l book.book_to.utc, format: :short}"
      }
      bookings << booking_data
    end
    render json: bookings
  end

  def timeline_data
    rooms = Array.new

    # Iterate Rooms
    Room.all.each do |room|
      bookings = Array.new

      # Iterate bookings
      room.room_bookings.each do |book|
        booking_data = {
          from: "/Date.UTC(#{book.book_from.to_time})/",
          to: "/Date.UTC(#{book.book_to.to_time})/",
          label: "#{book.id}",
          desc: "Od: #{l book.book_from} <br> Do: #{l book.book_to}"
        }
        bookings << booking_data
      end

      room_data = {
        name: room.name,
        values: bookings
      }

      rooms << room_data
    end

    render json: rooms
  end

  def destroy
    @booking.destroy
    respond_to do |format|
      format.html { redirect_to admin_bookings_path, notice: 'Rezervace odstraněna.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

end
