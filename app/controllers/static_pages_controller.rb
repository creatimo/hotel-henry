class StaticPagesController < ApplicationController
  def homepage
    @homepage = Page.where("key LIKE ? AND web = ? AND language = ?", "homepage", Page.webs[Rails.application.secrets.booking_type], Page.languages[I18n.locale]).first
  end

  def contacts
  end

  def spa
  end

  def restaurant
  end

  def rooms
  end
end
