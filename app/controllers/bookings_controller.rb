class BookingsController < ApplicationController
  before_action :set_booking, only: [:show, :edit, :update, :destroy]

  def calculate
    price = {
      status: "ok",
      all_night: params[:nightbooking],
      price: price_sum(params[:room], params[:date_from], params[:hours], params[:date_to], params[:nightbooking])
    }
    respond_to do |format|
      if true
        format.json { render json: price, status: :ok }
      else
        format.json { render json: price, status: :unprocessable_entity }
      end
    end
  end

  def price_sum(room, date_from, hours, date_to, nightbooking)
    if Rails.application.secrets.booking_type == 'hours'
      price = calculate_hours(room, hours, nightbooking)
    else
      nights = (date_to.to_date - date_from.to_date).to_i
      price = calculate_standard(room, nights)
    end

    return price
  end

  def calculate_hours(room, hours, nightbooking)
    room_detail = Room.find(room)
    if nightbooking == "1"
      price = room_detail.price_night.to_i
    else
      price = room_detail.price_first_hour.to_i + (room_detail.price_next_hour.to_i * (hours.to_i - 1))
    end
    return price
  end

  def calculate_standard(room, nights)
    room_detail = Room.find(room)
    price = room_detail.price_night.to_i * nights
    return price
  end

  # GET /bookings
  # GET /bookings.json
  def index
    @bookings = Booking.all
  end

  # GET /bookings/1
  # GET /bookings/1.json
  def show
  end

  # GET /bookings/new
  def new
  end

  # POST /bookings/rooms
  def room

    if Rails.application.secrets.booking_type == 'hours'
      rooms = Room.all.order('name ASC')

      if params[:nightbooking] == "1"
        hours = Rails.application.secrets.hour_night_hours.to_i
        hour_from = Rails.application.secrets.hour_night_check_in.to_i
        minute_from = 00
      else
        hours = params[:hours].to_i
        hour_from = params[:hour_from].to_i
        minute_from = params[:minute_from].to_i
      end

      book_from = params[:date_from].to_datetime.advance(:hours => hour_from, :minutes => minute_from)
      book_to = book_from.advance(:hours => hours)
    else
      rooms = Room.where("room_type <> ?", Room.room_types[:service]).order('name ASC')

      book_from = params[:date_from].to_datetime.advance(:hours => Rails.application.secrets.standard_check_in.to_i)
      book_to = params[:date_to].to_datetime.advance(:hours => Rails.application.secrets.standard_check_out.to_i)
    end

    if book_from.advance(hours: -2) < Time.now
      redirect_to root_path, alert: 'Nelze vytvořit rezervaci v minulosti'
    end

    # Gap check
    book_from_gap = book_from.advance(minutes: -30)
    book_to_gap = book_to.advance(minutes: +30)

    @bookings = RoomBooking.where("book_from < ? AND book_to > ?", book_to_gap, book_from_gap).pluck(:room_id)

    rooms_data = Array.new
    rooms.map { |room|
      room.name = room.name + ' - obsazeno' if @bookings.include? room.id
      rooms_data << room
    }

    @rooms = rooms_data

  end

  # GET /bookings/success
  def success
  end

  # GET /bookings/1/edit
  def edit
  end

  # POST /bookings
  # POST /bookings.json
  def create
    if compose_booking(params)
      redirect_to :success_booking
    else
      render :new
    end
  end

  # PATCH/PUT /bookings/1
  # PATCH/PUT /bookings/1.json
  def update
    respond_to do |format|
      if @booking.update(booking_params)
        format.html { redirect_to @booking, notice: 'Booking was successfully updated.' }
        format.json { render :show, status: :ok, location: @booking }
      else
        format.html { render :edit }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_booking
      @booking = Booking.find(params[:id])
    end

    def booking_params
      params.require(:booking).permit(:user_id, :message, :interenal_note)
    end

    def compose_booking(params)
      @booking = Booking.new

      @booking.user_id = get_user_id(params[:phone], params[:email])
      @booking.message = params[:message]
      @booking.save

      create_room_booking(@booking.id, params)

      BookingMailer.new_booking(@booking).deliver

      return @booking
    end

    def get_user_id(phone, email = '')
      user_by_phone = User.find_for_authentication(:phone => phone)
      user_by_email = User.find_for_authentication(:email => email)
      if user_by_phone
        return user_by_phone.id.to_i
      elsif user_by_email
        return user_by_email.id.to_i
      else
        @user = create_user(phone, email)
        return @user.id.to_i
      end
    end

    def create_user(phone, email)
      require 'securerandom'
      random_password = SecureRandom.hex

      if email == ''
        email = "#{random_password}@hotel.henry"
      end

      user = User.new(:email => email, :phone => phone, :password => random_password, :password_confirmation => random_password)
      user.save!
      return user
    end

    def create_room_booking(booking_id, params)

      if Rails.application.secrets.booking_type == 'hours'
        if params[:nightbooking] == "1"
          hours = Rails.application.secrets.hour_night_hours.to_i
          hour_from = Rails.application.secrets.hour_night_check_in.to_i
          minute_from = 00
        else
          hours = params[:hours].to_i
          hour_from = params[:hour_from].to_i
          minute_from = params[:minute_from].to_i
        end

      book_from = params[:date_from].to_datetime.advance(:hours => hour_from, :minutes => minute_from)
      book_to = book_from.advance(:hours => hours)

      else
        book_from = params[:date_from].to_datetime.advance(:hours => Rails.application.secrets.standard_check_in.to_i)
        book_to = params[:date_to].to_datetime.advance(:hours => Rails.application.secrets.standard_check_out.to_i)
      end

      @room_booking = RoomBooking.new
      @room_booking.booking_id = booking_id
      @room_booking.book_from = book_from
      @room_booking.book_to = book_to
      @room_booking.room_id = params[:room_id]
      @room_booking.save
      return @room_booking
    end
end
