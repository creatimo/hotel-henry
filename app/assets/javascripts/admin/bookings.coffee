container = document.getElementById('timeline')

groups = {}
items = {}

groupsRequest = $.getJSON('/timeline_groups.json', (data) ->
  groups = data
)
itemsRequest = $.getJSON('/timeline_items.json', (data) ->
  items = data
)

options =
  orientation:
    axis: 'both'
  zoomMax: 1000*60*60*24*10
  zoomMin: 1000*60*60*12
  stack: false

$.when(groupsRequest, itemsRequest).then ->
  timeline = new (vis.Timeline)(container, items, groups, options)
