$ () ->
  $('#datepicker').datepicker
    language: "cs"
    format: "yyyy-mm-dd"
    todayHighlight: true
    startDate: Date()
    onSelect: (dateText) ->
      console.log 'onselect'

  $('#datepicker').on 'changeDate', (event) ->
    $('#date_from').val $('#datepicker').datepicker('getFormattedDate')


  $('a[data-toggle="tab"]').on 'shown.bs.tab', (e) ->
    href = $(e.target).attr('href')
    if href == '#night'
      $('#nightbooking').val 1
    else
      $('#nightbooking').val 0


  calculate = ->
    $('#total-price').html 'počítám..'
    $.ajax
      url: '/calculate'
      data:
        room: $('#room_id').val()
        date_from: $('#date_from').val()
        hours: $('#hours').val()
        date_to: $('#date_to').val()
        nightbooking: $('#nightbooking').val()
      dataType: 'json'
      success: (data) ->
        console.log data
        $('#total-price').html data.price

  if $('#room_id').length > 0
    calculate()

  $('#room_id').change ->
    calculate()


  box = $('#mail-type')
  box.hide()
  $('#contact_prefer').change (event) ->
    type = $(this).val()
    if type == '2'
      box.slideDown()
    else
      box.slideUp()
