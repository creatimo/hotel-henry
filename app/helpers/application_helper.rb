module ApplicationHelper
  def active_locale(locale)
    'active' if I18n.locale == locale
  end

  def page_by_key(key)
    return Page.where(key: key)
            .where(language: Page.languages[I18n.locale])
            .where(web: Page.webs[Rails.application.secrets.booking_type])
            .first
  end

  def content_by_key(key)
    page = page_by_key(key)
    if !page.nil?
      page.content
    end
  end
end
