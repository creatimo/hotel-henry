module PagesHelper
  def link_to_page(key)
    return link_to t("menu.#{key}"), "/#{I18n.locale}/hh/#{key}"
  end
end
