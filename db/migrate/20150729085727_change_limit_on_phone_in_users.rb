class ChangeLimitOnPhoneInUsers < ActiveRecord::Migration
  def change
    change_column :users, :phone, :string, :limit => 16
  end
end
