class AddNightPriceToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :price_night, :integer
  end
end
