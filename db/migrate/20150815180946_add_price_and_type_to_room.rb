class AddPriceAndTypeToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :price_first_hour, :integer
    add_column :rooms, :price_next_hour, :integer
    add_column :rooms, :type, :integer
  end
end
