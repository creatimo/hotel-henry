class ChangeLanguageInPages < ActiveRecord::Migration
  def change
    rename_column :pages, :language_id, :language
  end
end
