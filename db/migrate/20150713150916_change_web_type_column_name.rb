class ChangeWebTypeColumnName < ActiveRecord::Migration
  def change
    rename_column :pages, :web_id, :web
  end
end
