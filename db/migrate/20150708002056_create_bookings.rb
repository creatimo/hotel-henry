class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :user_id
      t.text :message
      t.text :interenal_note

      t.timestamps null: false
    end
  end
end
