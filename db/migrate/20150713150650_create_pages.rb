class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :perex
      t.text :content
      t.integer :language_id
      t.integer :web_id
      t.string :key

      t.timestamps null: false
    end
  end
end
